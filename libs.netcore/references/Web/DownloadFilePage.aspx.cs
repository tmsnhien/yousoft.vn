﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\01_Framework\05_XafCustom\YouSoft.Vn.XAFCustom.Web\Pages\DownloadFilePage.aspx.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.IO;

namespace YouSoft.Vn.XAFCustom.Web.Pages
{
    public partial class DownloadFilePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string imageName = Request.QueryString["imageName"];
            if (!String.IsNullOrEmpty(imageName))
            {
                ImageInfo imageInfo = ImageLoader.Instance.GetImageInfo(imageName);
                if (imageInfo != null && imageInfo.Image != null)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        imageInfo.Image.Save(ms, ImageFormat.Jpeg);
                        ms.Seek(0, SeekOrigin.Begin);
                        byte[] imageContent = ms.ToArray();
                        Response.Clear();
                        Response.ClearHeaders();
                        Response.ClearContent();
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + imageName + ".jpg");
                        Response.AddHeader("Content-Length", imageContent.Length.ToString());
                        Response.ContentType = "image/jpeg";
                        Response.Flush();
                        Response.OutputStream.Write(imageContent, 0, imageContent.Length);
                        Response.End();
                    }
                }
            }
        }
    }
}